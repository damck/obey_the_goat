#!/usr/bin/python3
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest


class LayoutAndStylingTest(FunctionalTest):

    def test_layout_and_styling(self):
        # User goes to home page
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        # He sees the input box in the center
        inputBox = self.get_item_input_box()
        self.assertAlmostEqual(
            inputBox.location['x'] + inputBox.size['width'] / 2,
            512,
            delta=10
        )

        # He starts a new list and sees input is nicely centered too
        inputBox.send_keys('testing')
        inputBox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: testing')
        inputBox = self.get_item_input_box()
        self.assertAlmostEqual(
            inputBox.location['x'] + inputBox.size['width'] / 2,
            512,
            delta=10
        )
