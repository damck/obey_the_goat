#!/usr/bin/python3

from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest


class ItemValidationTest(FunctionalTest):

    def get_error_element(self):
        return self.browser.find_element_by_css_selector('.has-error')

    def test_cannot_add_empty_list_items(self):
        # user goes to home page and accidentally submits empty item
        # hitting enter absent-minded
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys(Keys.ENTER)

        # the page refreshes and there is an error message
        # that says item cannot be blank
        self.wait_for(lambda:
                      self.browser.find_element_by_css_selector('#id_text:invalid'))

        # he tries again with some text and now it works
        self.get_item_input_box().send_keys('Buy cookies')
        self.wait_for(lambda:
                      self.browser.find_element_by_css_selector('#id_text:valid'))
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy cookies')

        # now he tries to add another blank item
        self.get_item_input_box().send_keys(Keys.ENTER)

        # but is greeted with same error message
        self.wait_for_row_in_list_table('1: Buy cookies')
        self.wait_for(lambda:
                      self.browser.find_element_by_css_selector('#id_text:invalid'))

        # so he adds another correct item instead
        self.get_item_input_box().send_keys('Eat cookie')
        self.wait_for(lambda:
                      self.browser.find_element_by_css_selector('#id_text:valid'))
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy cookies')
        # self.wait_for_row_in_list_table('2: Eat cookie')

    def test_cannot_add_duplicate_items(self):
        # User goes to home page and adds a new list
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys("Buy wellies")
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy wellies')

        # By accident, he tries to add same thing again
        self.get_item_input_box().send_keys("Buy wellies")
        self.get_item_input_box().send_keys(Keys.ENTER)

        # He sees a helpful reminder that's not ok
        self.wait_for(lambda: self.assertEqual(
            self.get_error_element().text,
            "You've already got this in your list"
        ))

    def test_error_messages_are_cleared_on_input(self):
        # User starts a list and causes a validation error
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys("Banter too thick")
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Banter too thick')

        self.get_item_input_box().send_keys("Banter too thick")
        self.get_item_input_box().send_keys(Keys.ENTER)

        self.wait_for(lambda: self.assertTrue(
            self.get_error_element().is_displayed()
        ))

        # He starts typing in the input box to clear the error
        self.get_item_input_box().send_keys("a")

        # He notices the error message disappears
        self.wait_for(lambda: self.assertFalse(
            self.get_error_element().is_displayed()
        ))
