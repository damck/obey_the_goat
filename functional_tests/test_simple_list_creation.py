#!/usr/bin/python3
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from .base import FunctionalTest


class NewVisitorTest(FunctionalTest):

    def test_can_start_a_list_for_one_user(self):
        # Start a browser
        self.browser.get(self.live_server_url)

        # Assert its title is 'To-do'
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # Create a to-do item
        inputBox = self.get_item_input_box()
        self.assertEqual(
                inputBox.get_attribute('placeholder'),
                'Enter a to-do item'
        )

        # Type in "Buy cookies" into the text box
        inputBox.send_keys('Buy cookies')

        # By hitting enter the page updates
        # And the page now lists "1: cookies" as an item
        inputBox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy cookies')
        last_user_url = self.browser.current_url
        self.assertRegex(last_user_url, '/lists/.+')

        # There's another text box to add new item
        # Add "Eat cookies" to list
        inputBox = self.get_item_input_box()
        inputBox.send_keys('Eat cookies')
        inputBox.send_keys(Keys.ENTER)

        # After update page shows both items
        self.wait_for_row_in_list_table('1: Buy cookies')
        self.wait_for_row_in_list_table('2: Eat cookies')



    def test_multiple_users_can_start_lists_at_different_urls(self):
        # Start a browser
        self.browser.get(self.live_server_url)

        # Create a to-do item
        inputBox = self.get_item_input_box()
        inputBox.send_keys('Buy cookies')
        inputBox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy cookies')

        # User receives unique URL
        last_user_url = self.browser.current_url
        self.assertRegex(last_user_url, '/lists/.+')

        # New user uses the site

        # We use a new browser session to make sure
        # no cookies are interfering
        self.browser.quit()
        self.browser = webdriver.Firefox()

        # New user visits the home page, there is no sign
        # of last user's list
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy cookie', page_text)
        self.assertNotIn('Eat cookie', page_text)

        # New user starts a new list
        inputBox = self.get_item_input_box()
        inputBox.send_keys('Buy milk')
        inputBox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy milk')

        # New user gets his own unique URL
        new_user_url = self.browser.current_url
        self.assertRegex(new_user_url, '/lists/.+')
        self.assertNotEqual(new_user_url, last_user_url)

        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy cookie', page_text)
        self.assertNotIn('Eat cookie', page_text)

        # We call it quits

