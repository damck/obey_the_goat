"""superlists URL Configuration """

from django.conf.urls import url, include

from accounts import urls as accounts_urls
from lists import urls as lists_urls
from lists import views as lists_views

urlpatterns = [
    url(r'^$', lists_views.home_page, name='home'),
    url(r'^lists/', include(lists_urls)),
    url(r'^accounts/', include(accounts_urls)),

    # url(r'^admin/', admin.site.urls),
]
